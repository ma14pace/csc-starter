# CSC Starter

This is a sample app, showing off the structure of Conatiner Smart CI apps.
It is intendedd to be used as a starter application.

Two services, `svc1` und `svc2` have been sketched. Inside of our network, they
are available as

* [https://stp-test.wien.gv.at/csc-starter/svc1/](https://stp-test.wien.gv.at/csc-starter/svc1/)
* [https://stp-test.wien.gv.at/csc-starter/svc2/](https://stp-test.wien.gv.at/csc-starter/svc2/)

Service names, as defined in `docker-compose.yml`, get to be parts of URLs. There are
ways to customize the URLs, but in general, it is a good idea to stick to defaults.
