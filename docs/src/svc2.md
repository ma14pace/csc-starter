# Service 2

This service is another simple web page, served by an NGINX. 
Replace it with something useful, or delete the service and 
its directory, if the application has only one service.
