#!/usr/bin/env sh

echo "### Running lifecycle hook '$HOOK_NAME', hook type is '$HOOK_TYPE'"

if [ "$HOOK_TYPE" == "post-install" -o "$HOOK_TYPE" == "post-upgrade" ] ; then
  # call them by their private names and ports
  curl --head docs.docker:80/ || exit 1
  curl --head svc1.docker:80/ || exit 1
  curl --head svc2.docker:80/ || exit 1
fi

exit 0
