#   CSC Starter

This repository is a template for new repositories created with 
[Container Smart CI](https://stp.wien.gv.at/container-smart-ci/docs/).
Clone it, remove its remotes and connect it with a new repository.

The "application" is a basic starter, consisting of two dummy services 
called "svc1" and "svc2". These are implemented by [NGINX](https://github.com/nginx/nginx),
showing a start page.

We have a [docker-compose.yml](./docker-compose.yml)and one directory for each of the three services, containing the service's
[Dockerfile](./svc1/Dockerfile). There is also a test script
[ci_test/test.sh](./ci_test/test.sh), that will be executed on the
CI server. That's it.

## Creating a new repository from this starter

* Read the docs of [Container Smart CI](https://stp.wien.gv.at/container-smart-ci/docs/)

* Decide upon a _Standard Name_. Let's assume we want to create a cool tool, 
  and we therefore decide for a standard name of `cool-tool`.

* Create the new repository `cool-tool` in Bitbucket. Leave it empty, don't 
  add a README. You should see instructions titled _Let's put some bits in your bucket_.
  Keep it open, but don't do it now.

* Check out [this starter](https://bitbucket.org/ma14pace/csc-starter/) 
  with `git clone https://bitbucket.org/ma14pace/csc-starter.git cool-tool`
  under the new name (here: `cool-tool`)

* Change into that new directory: `cd cool-tool`

* Remove the old remote: `git remote remove origin`

* Add the new remote: `git remote add origin https://bitbucket.org/ma14pace/cool-tool.git`,
  replacing `cool-tool` with the name of your project

* Add another remote to a development repo (optional). This might make sense if you
  have your own git repo and use ours only for pushing to our CI server.

* Change the image names in [docker-compose.yml](docker-compose.yml) from
  `magwien/csc-starter-XXX` to 
  `magwien/cool-tool-XXX` (where XXX is the service name)

* Commit changes, push to Bitbucket

* Push to the new repository: `git push -u origin master`

## Testing

### Locally

From the root directory of the checked out repo:

```
% docker-compose up -d
Creating network "cool-tool_default" with the default driver
Building docs
[+] Building 2.7s (17/17) FINISHED
Building svc1
[+] Building 0.4s (8/8) FINISHED
Building svc2
[+] Building 0.4s (8/8) FINISHED
Creating cool-tool_svc1_1 ... done
Creating cool-tool_docs_1 ... done
Creating cool-tool_svc2_1 ... done
Attaching to cool-tool_svc1_1, cool-tool_docs_1, cool-tool_svc2_1
%
% ./ci_test/test.sh
### http://127.0.0.1:8000/index.html
HTTP/1.1 200 OK

### SUCCESS
%
% docker-compose down --rmi all
Stopping cool-tool_docs_1 ... done
Stopping cool-tool_svc2_1 ... done
Stopping cool-tool_svc1_1 ... done
Removing cool-tool_docs_1 ... done
Removing cool-tool_svc2_1 ... done
Removing cool-tool_svc1_1 ... done
Removing network cool-tool_default
Removing image magwien/cool-tool-docs:dev
Removing image magwien/cool-tool-svc1:dev
Removing image magwien/cool-tool-svc2:dev
%
```

### On a test server

* Ask project management to make the app available via Standard Portal. 
  In this case, three exposed services, this would be like

| Parameter     | Value                                                      |
|---------------|------------------------------------------------------------|
  | app name      | `cool-tool`                                                |
  | target-prefix | `https://lxdockertest1.host.magwien.gv.at:8443/cool-tool/` |
  | role regex    | `^/(\w[^/]+)`                                              |
  | portal role   | `docs`                                                     |
  | authenticated | `yes`                                                      |
  | portal role   | `svc1`                                                     |
  | authenticated | `yes`                                                      |
  | portal role   | `svc2`                                                     |
  | authenticated | `yes`                                                      |

  One group assigned to all roles, with all users and - if needed - client
  certificates added. 
  
* Ask project management to set up Bitbucket web hook and Jenkins project

* Ask project management to set up automatic deployment to the test server 
  via Ansible Tower

* Ask project management to manually start a first build

* If the CI project is configured correctly, this will result in a mail
  reporting SUCCESS or FAILURE (with log)

* If automatic deployment was configured, the application will be installed 
  on the test server
  
* See the result on https://stp-test.wien.gv.at/cool-tool/app. At that 
  point, the app is only visible inside our network.

* If developers need access from outside our network, this has to be 
  configured

## Before you do anything else

Change the content of this [README](README.md). There is nothing sadder than
a readme speaking of the wrong project :)
